%% RADIO Deliverable Class
%% 
%% This is file `radio.cls', 
%% Copyright 2015, Antonis Troumpoukis <antru@iit.demokritos.gr>
%%
%% This file is based on the 'article.cls' LaTeX2e class, 
%% copyright of the LaTeX project. See http://www.latex-project.org/
%% on how to get the complete LaTeX distribution.
%%
%% It may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2003/12/01 or later.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{radio}[2015/05/06 Radio deliverable class] 

\LoadClass[a4paper,oneside,11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Definitions
%%

\RequirePackage{times}
\RequirePackage{graphicx}
\RequirePackage{titling}
\RequirePackage{xifthen}
\RequirePackage{array}
\RequirePackage{tabularx}
\RequirePackage[usenames,dvipsnames]{color}
\RequirePackage[svgnames,table]{xcolor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Margins of Text Pages
%%
\RequirePackage[%
  a4paper,
  top=1in,   bottom=0.9in,     left=1in,    right=1in, 
  bindingoffset=0cm, 
  head=1in,  headsep=0.3in, 
  foot=1in,  footskip=0.4in ]{geometry}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Macros
%

\def\thedeliverable{0.0}
\newcommand{\deliverable}[1]{
  \def\thedeliverable{#1}}

\def\theversion{0.0}
\newcommand{\version}[1]{
  \def\theversion{#1}}

\newcommand{\level}[1]{
  \def\thelevel{#1}}

\newcommand{\due}[1]{
  \def\thedue{#1}}
  
\newcommand{\submission}[1]{
  \def\thesubmission{#1}}

\newcommand{\workpackage}[1]{
  \def\theworkpackage{#1}}

\newcommand{\task}[1]{
  \def\thetask{#1}}
  
\newcommand{\lead}[1]{
  \def\thelead{#1}}

\newcommand{\contr}[1]{
  \def\thecontr{#1}}
  
\newcommand{\type}[1]{
  \def\thetype{#1}}

\newcommand{\status}[1]{
  \def\thestatus{#1}}

\renewcommand{\abstract}[1]{
  \def\theabstract{#1}}
  
\newcommand{\frontmatter}{
  \pagenumbering{roman}}

\newcommand{\mainmatter}{
  \newpage
  \setcounter{page}{1}
  \pagenumbering{arabic}
  
  \arrayrulecolor{black}
   \renewcommand\arraystretch{1.5}
  }

\newcommand{\specialsection}[1]{
  {\fontsize{15pt}{15pt}\selectfont{\textcolor{myblue}{#1}}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Title Page
%

\definecolor{myblue}{RGB}{65, 138, 179}
\definecolor{mylblue}{RGB}{176, 208, 226}
\definecolor{myblblue}{RGB}{108, 168, 202}
\definecolor{myllblue}{RGB}{215, 231, 240}
\definecolor{mygreen}{RGB}{166, 183, 39}
\definecolor{mygrey}{RGB}{128, 128, 128}
\definecolor{mylgrey}{RGB}{166, 166, 166}
\definecolor{mydgrey}{RGB}{94, 94, 94}

\newcommand*{\arraycolor}[1]{\protect\leavevmode\color{#1}}

\renewcommand{\maketitle}{%
      
      \setcounter{page}{0}
      
      \thispagestyle{empty}
      
      \begin{minipage}{0.38\textwidth}
	\includegraphics[height=1.45in]{radio-logo.png}
      \end{minipage}% This must go next to `\end{minipage}`
      \begin{minipage}{0.7\textwidth}
	{\fontsize{14pt}{14pt}\selectfont{\textcolor{mygreen}{ROBOTS IN ASSISTED \\ LIVING ENVIRONMENTS}}}
	
	\vspace*{11pt}
	
	{\fontsize{11pt}{11pt}\selectfont{\textcolor{mygreen}{UNOBTRUSIVE, EFFICIENT, RELIABLE AND MODULAR \\ SOLUTIONS FOR INDEPENDENT AGEING}}}
      \end{minipage}
      
      \vspace*{40pt}
      
      {\fontsize{14pt}{14pt}\selectfont{\textcolor{mygreen}{Research Innovation Action}}}
      
      \textcolor{mygrey}{Project Number:  643892} \hfill 
      \textcolor{mygrey}{Start Date of Project: 01/04/2015} \hfill 
      \textcolor{mygrey}{Duration: 36 months}
      
      \vspace*{28pt}
      
      {\fontsize{28pt}{28pt}\selectfont{\textcolor{myblue}{DELIVERABLE \thedeliverable}}}
      
      \vspace*{28pt}
      
      {\fontsize{28pt}{20pt}\selectfont{\textcolor{myblue}{\thetitle}}}
      
      \vspace*{28pt}
      
      { \centering
	\renewcommand{\arraystretch}{1.3}
	
	\rowcolors[\hline]{1}{myllblue}{mylblue}{}
	\arrayrulecolor{myblblue}
	\arrayrulewidth=1pt
	
	\begin{tabular*}{\textwidth}{|m{0.30\textwidth}|m{0.6405\textwidth}|}
	
	  \ifthenelse{\isundefined{\thelevel}}{}
	    {\arraycolor{White}\bfseries Dissemination Level \cellcolor{myblue} & {\bf \thelevel} \\ }
	  
	  \ifthenelse{\isundefined{\thedue}}{}
	    {\arraycolor{White}\bfseries Due Date of Deliverable \cellcolor{myblue} & \thedue \\ }
	  
	  \ifthenelse{\isundefined{\thesubmission}}{}
	    {\arraycolor{White}\bfseries Actual Submission Date \cellcolor{myblue}  & \thesubmission \\ }
	  
	  \ifthenelse{\isundefined{\theworkpackage}}{}
	    {\arraycolor{White}\bfseries Work Package \cellcolor{myblue}  & \theworkpackage \\ }
	  
	  \ifthenelse{\isundefined{\thetask}}{}
	    {\arraycolor{White}\bfseries Task \cellcolor{myblue}  & \thetask \\ }
	    
	  \ifthenelse{\isundefined{\thelead}}{}
	    {\arraycolor{White}\bfseries Lead Beneficiary \cellcolor{myblue}  & \thelead \\ }
	    
	  \ifthenelse{\isundefined{\thecontr}}{}
	    {\arraycolor{White}\bfseries Contributing Beneficiaries \cellcolor{myblue}  & \thecontr \\ }
	  
	  \ifthenelse{\isundefined{\thetype}}{}
	    {\arraycolor{White}\bfseries Type \cellcolor{myblue}  & \thetype \\ }
	  
	  \ifthenelse{\isundefined{\thestatus}}{}
	    {\arraycolor{White}\bfseries Status \cellcolor{myblue}  & \thestatus \\ }
	  
	  \arraycolor{White}\bfseries Version \cellcolor{myblue}  & \theversion
	\end{tabular*}
      }
      
      \vfill
      
      \begin{center}
	\includegraphics[height=0.34in]{h2020.png}

	{\fontsize{10pt}{10pt}\selectfont{\textcolor{mylgrey}{Project funded by the European Union’s Horizon 2020 Research and Innovation Actions}}}
      \end{center}
      
      \newpage
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% History, Authors, Schedule
%

\newcommand{\documentinfo}{%
  
  \specialsection{Abstract}
  
  \theabstract
  
  \vspace*{20pt}
  
  \specialsection{History}
  
  { \centering
    \renewcommand{\arraystretch}{1.1}
    
    \rowcolors[\hline]{2}{white}{myllblue}{}
    \arrayrulecolor{myblblue}
    \arrayrulewidth=1pt
    
    \begin{tabular*}{\textwidth}{|m{0.10\textwidth}|m{0.10\textwidth}|m{0.4828\textwidth}|m{0.20\textwidth}|}
    
      \rowcolor{myblue}
      \arraycolor{White}\bfseries Version & \arraycolor{White}\bfseries Date & \arraycolor{White}\bfseries Reason & \arraycolor{White}\bfseries Revised by   \\
	
      [...] & [...] & [...] & [...] \\
      
      [...] & [...] & [...] & [...] \\
      
      [...] & [...] & [...] & [...] \\
      
      [...] & [...] & [...] & [...] \\
      
    \end{tabular*}
  }
  
  \newpage
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Headers and Footers
%

\RequirePackage{fancyhdr}
\RequirePackage{etoolbox}

\newcommand{\headrulecolor}[1]{\patchcmd{\headrule}{\hrule}{\color{#1}\hrule}{}{}}

\pagestyle{fancy}
  \fancyhf{}
  \fancyhead[R]{\textcolor{myblue}{D\thedeliverable \,- v\theversion}}
  \fancyhead[L]{}
  \fancyfoot[R]{\fontsize{10pt}{12pt}\selectfont\thepage}
  \fancyfoot[C]{}
  \fancyfoot[L]{}
  \headrulecolor{mylblue}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
%
\renewcommand{\subparagraph}{}

\RequirePackage{parskip}
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt}

\RequirePackage[compact]{titlesec}

\newcommand{\sectionbreak}{\clearpage}

\titleformat{\section}
  {\vspace*{8pt}\LARGE\sf\bfseries\scshape\raggedright}
  {\thesection}{1em}{}
  [\titlerule]
  
\titleformat{\subsection}
  {\large\sf\bfseries\raggedright}
  {\thesubsection}{1em}{}

\titleformat{\subsubection}
  {\bfseries\sf\raggedright}
  {\thesubsubsection}{1em}{}

\RequirePackage{titlesec}
\titlespacing\section{0pt}{0pt plus 0pt minus 0pt}{5pt plus 0pt minus 0pt}
\titlespacing\subsection{0pt}{4pt plus 0pt minus 0pt}{0pt plus 0pt minus 0pt}
\titlespacing\subsubsection{0pt}{0pt plus 0pt minus 0pt}{0pt plus 0pt minus 0pt}

\usepackage[font={small,color=mydgrey,it},skip=8pt,justification=centering]{caption}




